from django.conf import settings
from django.db import models
from django.db.models import CharField
from django.utils import timezone
from django.db.models.signals import post_save

  
class Categoria(models.Model):
    id_categoria = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)

    class Meta:
        verbose_name= 'Categoria'
        verbose_name_plural= 'Categorias'
        ordering =['id_categoria']

    def __str__(self):
        return  self.nombre

class Producto(models.Model):
    id_producto = models.AutoField(primary_key=True) #AutoField es un campo autoincremental 
    precio = models.DecimalField(max_digits=6, decimal_places=2)
    nombre = models.CharField(max_length=120)
    descripcion = models.TextField()
    img = models.ImageField(upload_to='img')
    id_categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)

    class Meta:
        verbose_name= 'Producto'
        verbose_name_plural= 'Productos'
        ordering =['id_producto']

    def __str__(self):
        return self.nombre


class Usuario(models.Model):
    usuario = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, help_text= "El usuario debe contener entre 6 a 10 caracteres")
    nombre = models.CharField(max_length=45)
    apellido = models.CharField(max_length=45)
    direccion = models.CharField(max_length=100)
    password = models.CharField(max_length=20, help_text= "La contraseña debe contener entre 8 a 16 caracteres")
    telefono = models.CharField(max_length=15)
    email = models.EmailField()
    prods= models.ManyToManyField(Producto, blank=True)

    class Meta:
        verbose_name= 'Usuario'
        verbose_name_plural = 'Usuarios'
        ordering= ['usuario', 'password']

    def __str__(self):
        return  '%s %s' %(self.usuario, self.password)



class Item_Pedido(models.Model):
    producto = models.OneToOneField(Producto, on_delete=models.SET_NULL, null=True)
    is_ordered = models.BooleanField(default=False)
    date_added = models.DateTimeField(auto_now=True)
    date_ordered = models.DateTimeField(null=True)

    def __str__(self):
        return self.producto.nombre 


class Pedido(models.Model):
    id_pedido = models.AutoField(primary_key=True)
    cod_ref = models.CharField(max_length=15)
    descripcion = models.TextField()
    precio = models.DecimalField(max_digits=4, decimal_places=2)
    fecha_pedido = models.DateTimeField(default=timezone.now)
    estado = models.CharField(max_length=100)
    usuario= models.ForeignKey(Usuario, on_delete=models.CASCADE)
    items = models.ManyToManyField(Item_Pedido) #Se pone asi cuando hay una relacion de muchos a muchos

    class Meta:
        verbose_name= 'Pedido'
        verbose_name_plural= 'Pedidos'
        ordering =['fecha_pedido']
    
    def get_cart_items(self):
        return self.items.all()

    def get_cart_total(self):
        return sum([item.producto.precio for item in self.items.all()])

    def __str__(self):
        return '{0} - {1}'.format(self.usuario, self.cod_ref)
    




