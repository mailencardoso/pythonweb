from django.apps import AppConfig


class PedidosOnlineConfig(AppConfig):
    name = 'pedidos_online'
