from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout as do_logout
from django.contrib.auth.models import User
from .forms import LoginForm, SignUpForm
from django.contrib.auth.decorators import login_required
from pedidos_online.models import Producto, Pedido, Item_Pedido, Usuario 



# Create your views here.

def home(request):
    # La logica deberia ser algo asi para obtener y compara con la base de datos y despueés ver como enviarlo a la template pedidos.
    # def home(request,id):
    # producto_seleccionado=producto.objects.get(id_producto=id)
    produ=Producto.objects.all()
    return render(request, 'pedidos_online/home.html',{"produ":produ})


def login_view(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")

    return render(request, "pedidos_online/login.html", {"form": form})



def producto_lista(request):
    produ = Producto.objects.all()
    pedidos_filtrados = Pedido.objects.filter(usuario=request.user.usuario, is_ordered=False)
    pedido_productos_actual = []
    if pedidos_filtrados.exists():
        usuario_pedido = pedidos_filtrados[0]
        usuario_pedido_articulos = pedidos_filtrados.items.all()
        pedido_productos_actual = [producto.producto for producto in usuario_pedido_articulos]

    context = {
        'produ': produ,
        'pedido_productos_actual': pedido_productos_actual
    }

    return render(request, "pedidos_online/producto_lista.html", context)


@login_required
def producto(request):
    return render(request, 'pedidos_online/producto.html')


@login_required
def buscar(request):
    if request.GET["pro"]:

        #mensaje = " Articulo buscado:%r" % request.GET["pro"]
        producto = request.GET["pro"]
        articulos = Producto.objects.filter(descripcion__icontains=producto)

    return render(request, 'pedidos_online/producto.html', {"articulos": articulos, "query": producto})
    #else:

       # mensaje = "No has introducido ningun articulo"

    #return HttpResponse(mensaje)

def get_user_pending_order(request):
    # get order for the correct user
    user_profile = get_object_or_404(Usuario, user=request.usuario)
    pedido = Peido.objects.filter(usuario=user_profile, is_ordered=False)
    if pedido.exists():
        # get the only order in the list of filtered orders
        return pedido[0]
    return 0

@login_required()
def add_to_cart(request, **kwargs):
    # get the user profile
    user_profile = get_object_or_404(Usuario, user=request.usuario)
    # filter products by id
    producto = Producto.objects.filter(id=kwargs.get('item_id', "")).first()
    # check if the user already owns this product
    if producto in request.user.usuario.prods.all():
        messages.info(request, 'Ya tienes este producto')
        return redirect(reverse('products:product-list')) 
    # create orderItem of the selected product
    order_item, status = Item_Pedido.objects.get_or_create(producto=producto)
    # create order associated with the user
    user_order, status = Pedido.objects.get_or_create(usuario=user_profile, is_ordered=False)
    user_order.items.add(order_item)
    if status:
        # generate a reference code
        user_order.cod_ref = Pedido.objects.id_pedido
        user_order.save()

    # show confirmation message and redirect back to the same page
    messages.info(request, "Articulo agregado a su pedido")
    return redirect(reverse('products:product-list'))

@login_required()
def delete_from_cart(request, item_id):
    item_to_delete = Item_Pedido.objects.filter(pk=item_id)
    if item_to_delete.exists():
        item_to_delete[0].delete()
        messages.info(request, "Articulo eliminado")
    return redirect(reverse('shopping_cart:order_summary'))


@login_required()
def order_details(request, **kwargs):
    existing_order = get_user_pending_order(request)
    context = {
        'order': existing_order
    }
    return render(request, 'pedidos_online/order_summary.html', context)


@login_required
def pedidos(request,id_producto): 
#def pedidos(request):
   
   pedi = Producto.objects.get(id_producto=id_producto)
    
  
   

   return render(request, 'pedidos_online/pedidos.html',{"pedi":pedi})


#def register(request):
    
    #form = CreateUserForm()

    #if request.method == 'POST':
        #form = CreateUserForm(request.POST)
        #f form.is_valid():
            #form.save()
           
           
           # username = form.cleaned_data['username']
            #password = form.cleaned_data['password1']
            #user = authenticate(username=username, password=password)
            #login(request, user)
            #return redirect('home')
    #else:
        #form = UserCreationForm()

   #context = {'form': form}
   #return render(request, 'registration/register.html', context)


def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('/')
# def post_list(request):





